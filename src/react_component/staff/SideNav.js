import React from "react";
import { Link } from "react-router-dom";
import { Drawer} from "antd";
import "../../CSS/SideNav.scss";
import TopNav from "../TopNav";

export default class SideNav extends React.Component {
  constructor(props) {
    super(props);
    this.state = { visible: false };
  }

  showDrawer = () => {
    this.setState({
      visible: true
    });
  };

  onClose = () => {
    this.setState({
      visible: false
    });
  };

  render() {
    return (
      <div>
        <Drawer
          className="sideNavtt, abc"
          title="Basic Drawer"
          placement="left"
          closable={false}
          onClose={this.onClose}
          visible={this.state.visible}
        >
          <ul className="SideNav">
            <li>
              <Link to="/staff/myProfile">myProfile</Link>
            </li>
            <li>
              <Link to="/staff/leave">leave Requests</Link>
            </li>
          </ul>
        </Drawer>
        <TopNav showDrawer={this.showDrawer} />
      </div>
    );
  }
}
