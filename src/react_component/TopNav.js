import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Avatar, Spin } from 'antd';
import { connect } from 'react-redux';
import '../CSS/TopNav.scss';
import jwtDecode from 'jwt-decode';
import axios from 'axios';
import { logout } from './auth';
import signOut from '../Public/sign_out.svg';
import { avatarChange } from '../redux/action';

class TopNav extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hover: true, avatar: '', loading: true };
  }

  toggleHover = () => {
    this.setState({ hover: !this.state.hover });
  };

  componentDidMount() {
    const token = localStorage.jwt_token
      ? localStorage.jwt_token
      : sessionStorage.jwt_token;
    const { id } = jwtDecode(token);
    axios
      .get(`/profile/${id}`)
      .then(res => {
        const { avatar } = res.data;
        this.setState({ loading: false });
        this.props.dispatch(avatarChange({ avatar }));
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    return (
      <div className='TopNav'>
        <div style={{ display: 'flex' }}>
          <div onClick={this.props.showDrawer} className='Icon' />
          <div>
            <p>Insight</p>
          </div>
        </div>
        <div style={{ display: 'flex' }}>
          <Link to='/staff/myProfile'>
            <Spin spinning={this.state.loading}>
              <Avatar
                src={this.props.avatar}
                style={{
                  height: 66,
                  width: 66,
                  marginTop: 18,
                  borderRadius: '50%',
                  cursor: 'pointer'
                }}
              />
            </Spin>
          </Link>
          <div>
            <Link to='/'>
              <img
                alt='logout'
                className='Logout'
                src={signOut}
                style={{
                  height: 55,
                  marginLeft: 5,
                  marginTop: 27,
                  cursor: 'pointer'
                }}
                onClick={e => {
                  e.preventDefault();
                  logout();
                  this.props.history.replace('/');
                }}
              />
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  avatar: state.avatar
});

export default connect(mapStateToProps)(withRouter(TopNav));
