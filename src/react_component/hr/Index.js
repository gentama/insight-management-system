import React from "react";
import Nav from "./SideNav";
import Routes from "./route";
import ParticlesContainer from "./pages/Particles";

export default class HR extends React.Component {
  constructor(props) {
    super(props);
    this.state = { visible: false, timer: null };
  }

  onMouseMove = e => {
    const { timer } = this.state;
    if (!timer && e.clientX === 0) {
      this.setState({
        timer: setTimeout(() => {
          this.setState({ visible: true });
        }, 500)
      });
    }
    if (timer && e.clientX !== 0) {
      clearTimeout(timer);
      this.setState({ timer: null });
      this.setState({ visible: false });
    }
  };

  showDrawer = () => {
    this.setState({
      visible: true
    });
  };

  onClose = () => {
    this.setState({
      visible: false
    });
  };

  render() {
    return (
      <div>
        <Nav
          visible={this.state.visible}
          showDrawer={this.showDrawer}
          onClose={this.onClose}
        />
        <div onMouseMove={this.onMouseMove}>
          <div
            style={{
              position: "absolute",
              top: 100,
              left: 0,
              width: "100%",
              height: "100%",
              // zIndex:-1,
              opacity: 0.6
            }}
          >
            <ParticlesContainer />
          </div>
          <Routes />
        </div>
      </div>
    );
  }
}
