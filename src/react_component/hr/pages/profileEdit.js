import { Form, Row, Col, Input, Button } from "antd";
import React from "react";
import axios from "axios";

class AdvancedSearchForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      name: "",
      phone: "",
      email: "",
      department: "",
      role: ""
    };
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  submit = () => {
    this.setState({ loading: false });
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { name, phone, email, department, role } = values;
        const newName = name || this.props.name;
        const newPhone = phone || this.props.phone;
        const newEmail = email || this.props.email;
        const newDepartment = department || this.props.department;
        const newRole = role || this.props.role;
        axios
          .put(`/profile/${this.props.email}`, {
            name: newName,
            phone: newPhone,
            email: newEmail,
            department: newDepartment,
            role: newRole
          })
          .then(res => {
            console.log("succed");
            this.props.onEditArray(
              newEmail,
              newName,
              newPhone,
              newDepartment,
              newRole
            );
            this.props.handleOk();
          })
          .catch(err => {
            console.log("err");
            this.props.handleOk();
          });
      }
    });
  };

  getFields = () => {
    const { getFieldDecorator } = this.props.form;
    const children = [];
    const { name, phone, email, department, role } = this.props;
    const placeholder = [name, phone, email, department, role];
    const label = ["name", "phone", "email", "department", "role"];
    for (let i = 0; i < 5; i++) {
      children.push(
        <Col span={12} key={i}>
          <Form.Item label={label[i]}>
            {getFieldDecorator(`${label[i]}`, {
              rules: [
                {
                  required: false,
                  message: "Input something!"
                }
              ]
            })(<Input placeholder={placeholder[i]} />)}
          </Form.Item>
        </Col>
      );
    }
    return children;
  };

  render() {
    return (
      <Form className="ant-advanced-search-form" onSubmit={this.handleSearch}>
        <Row gutter={8}>
          {this.getFields()}
          <Button
            type="primary"
            size="large"
            onClick={this.submit}
            style={{ marginTop: 40, marginLeft: 70 }}
          >
            submit
          </Button>
        </Row>
      </Form>
    );
  }
}

const WrappedNormalLoginForm = Form.create({ name: "advanced_search" })(
  AdvancedSearchForm
);
export default WrappedNormalLoginForm;
