import React from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
// import calendarPlugins from '@fullcalendar/daygrid'
import interactionPlugin from "@fullcalendar/interaction";
import { DatePicker, Input, Button } from "antd";
import axios from "axios";
import { SliderPicker } from "react-color";

import "../../../CSS/dashboard.scss"; // webpack must be configured to do this

export default class DemoApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: [],
      event: [],
      input: "",
      color: "#e6b3b3"
    };
  }

  //  [] { id:'12312',title: 'event 1', start: '2019-09-21',end:'2019-09-23' },
  //       { title: 'event 2', date: '2019-09-22',editable:'true'}]

  onChange = date => this.setState({ date });

  onInputChange = e => {
    this.setState({ input: e.target.value });
  };

  handleColorChange = colorCode => {
    this.setState({ color: colorCode.hex });
  };

  onDateChange = (value, dateString) => {
    this.setState({ date: dateString });
  };

  onOk = value => {
    console.log("onOk: ", value);
    console.log(this.state.date);
  };

  componentDidMount() {
    const _this = this;
    axios
      .get("/dashboard/")
      .then(response => {
        _this.setState((pre, props) => {
          return { event: response.data };
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  submit = () => {
    const _this = this;
    const start = _this.state.date[0];
    const end = _this.state.date[1];
    axios
      .post("/dashboard/", {
        start: start,
        end: end,
        title: _this.state.input,
        backgroundColor: this.state.color,
        id: Math.floor(Math.random() * 1000)
      })
      .then(response => {
        alert("upload succeed");
        console.log(response.data);
        axios.get("/dashboard/").then(response => {
          // alert("fetch data succeed");
          console.log(response.data);
          _this.setState((pre, props) => {
            return { event: response.data };
          });
        });
      })
      .catch(function(err) {
        // alert("fetch data failed");
        console.log(err);
      });
  };

  render() {
    const { RangePicker } = DatePicker;
    return (
      <div>
        <form onSubmit={this.onFormSubmit}>
          <h3>Pick your event color:</h3>
          <SliderPicker
            color={this.state.color}
            onChange={this.handleColorChange}
            
          />
          <h3 style={{marginTop:20}}>Input your event:</h3>
          <Input
            name="Event"
            value={this.state.input}
            onChange={this.onInputChange}
            style={{marginBottom:20}}
          ></Input>
          <h3>Pick a time for your event:</h3>
          <RangePicker
            showTime={{ format: "HH:mm" }}
            format="YYYY-MM-DD"
            placeholder={["Start Time", "End Time"]}
            onChange={this.onDateChange}
            onOk={this.onOk}
          />
        </form>
        <Button type="primary" onClick={this.submit}>
          submit
        </Button>
        <div className="ggg">
        <FullCalendar
        
          defaultView="dayGridMonth"
          plugins={[dayGridPlugin, interactionPlugin]}
          weekends={true}
          editable={true}
          // startEditable={true}
          selectable={true}
          aspectRatio={2}
          events={this.state.event}
        />
        </div>
      </div>
    );
  }
}
