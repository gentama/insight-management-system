import React from "react";
import axios from "axios";
import { Table, Modal, Spin } from "antd";
import "../../../CSS/department.scss";
import more from "../../../Public/more.svg";

export default class Department extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      columns: [
        {
          title: "#",
          dataIndex: "_id",
          key: "_id",
          align: "center"
        },
        {
          title: "Department Name",
          dataIndex: "name",
          key: "name",
          align: "center"
        },
        {
          title: "Department Head",
          dataIndex: "head",
          key: "head",
          align: "center"
        },
        {
          title: "Total Employee",
          key: "employee",
          dataIndex: "employee",
          align: "center"
        },
        {
          title: "Action",
          key: "action",
          render: (text, record, index) => (
            <span>
              <img
              alt="more"
                src={more}
                style={{ height: 30, cursor: "pointer" }}
                onClick={() => {
                  this.onMore(text._id);
                }}
              />
            </span>
          )
        }
      ],
      profileData: [],
      profileColumns: [
        {
          title: "Name",
          dataIndex: "name",
          key: "name",
          align: "center",
          sorter: (a, b) => a.name.localeCompare(b.name),
        },
        {
          title: "role",
          dataIndex: "role",
          key: "role",
          align: "center",
          sorter: (a, b) => a.name.localeCompare(b.name)
        },
        {
          title: "phone",
          key: "phone",
          dataIndex: "phone",
          align: "center",
        }
      ],
      department: [],
      visible: false,
      id: "",
      name: "",
      head: "",
      loading: true,
      profileLoading: true
    };
  }

  onMore = id => {
    this.setState({ visible: true });
    axios.get(`/dep`).then(res => {
      const newData = res.data.filter(e => e._id === id)[0].profile;
      this.setState({ profileData: newData, profileLoading: false });
    });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  componentDidMount() {
    axios
      .get(`/dep`)
      .then(res => {
        const newData = res.data;
        newData.forEach(e => {
          e["employee"] = e.profile.length;
        });
        this.setState({ data: newData, loading: false });
      })
      .catch(err => {
        console.log(err);
      });
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleOk = e => {
    const { id, name, head } = this.state;
    axios
      .post(`/dep`, {
        id: id,
        name: name,
        head: head,
        key: id
      })
      .then(alert("succeed"))
      .catch(err => console.log(err));

    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false
    });
  };

  openModal = () => {
    this.setState({ visible: true });
  };
  render() {
    return (
      <div>
        <Spin spinning={this.state.loading}>
          <div >
            <Table
              columns={this.state.columns}
              dataSource={this.state.data}
              pagination={false}
              rowKey={record => record.uid}
            />
          </div>
        </Spin>

        <Modal
          title="New Department"
          visible={this.state.visible}
          onOk={this.handleCancel}
          onCancel={this.handleCancel}
        >
          <Spin spinning={this.state.profileLoading}>
            <Table
              columns={this.state.profileColumns}
              dataSource={this.state.profileData}
              pagination={false}
              rowKey={record => record.uid}
            />
          </Spin>
        </Modal>
      </div>
    );
  }

  // <Button
  //   onClick={this.openModal}
  //   style={{ marginTop: 30 }}
  //   type="primary"
  // >
  //   Add New Department
  // </Button>
}
