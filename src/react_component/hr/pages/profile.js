import React from "react";
import axios from "axios";
import { Modal, Table, Divider, Spin, Pagination, Avatar } from "antd";
import ProfileById from "./profilelistById";
import edit from "../../../Public/edit.svg";
import Delete from "../../../Public/delete.svg";
import ProfileEdit from "./profileEdit";
import "../../../CSS/profile.scss";

export default class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: [],
      searchByName: "",
      dataLoad: false,
      currentData: "",
      data: [],
      columns: [
        {
          title: "Avatar",
          dataIndex: "image",
          key: "image",
          render: (text, record, index) => (
            <Avatar
              src={record.avatar}
              shape="circle"
              size="large"
            ></Avatar>
          )
        },
        {
          title: "Name",
          dataIndex: "name",
          key: "name",
          sorter: (a, b) => a.name.localeCompare(b.name)
        },
        {
          title: "Email",
          dataIndex: "_id",
          key: "_id"
        },
        {
          title: "Phone",
          dataIndex: "phone",
          key: "phone"
        },
        {
          title: "Department",
          key: "department",
          dataIndex: "department",
          sorter: (a, b) => a.name.localeCompare(b.name)
        },
        {
          title: "Role",
          key: "role",
          dataIndex: "role"
        },
        {
          title: "Action",
          key: "action",
          render: (text, record, index) => (
            <span>
              <img
                src={edit}
                alt={"edit"}
                style={{ height: 37, cursor: "pointer" }}
                onClick={() => {
                  this.showModal(text);
                }}
              />
              <Divider type="vertical" />
              <img
                src={Delete}
                alt={"delete"}
                style={{ height: 35, cursor: "pointer" }}
                onClick={() => {
                  this.onDeleteClick(text._id);
                }}
              />
            </span>
          )
        }
      ],
      loading: true,
      pagination: [],
      pageSize: 5,
      page: 1,
      totalPages: ""
    };
  }
  // sort=(a,b)=>{

  // }


  componentDidMount() {
    axios
      .get(
        `/profile/?isPagination=yes&pageSize=${this.state.pageSize}&page=${this.state.page}`
      )
      .then(res => {
        this.setState({
          data: res.data.query,
          loading: false,
          totalPages: res.data.pages
        });
      })
      .catch(function(err) {
        console.log(err);
      });
  }

  showModal = e => {
    this.setState({
      visible: true,
      currentData: e,
      dataLoad: true
    });
  };

  handleOk = e => {
    this.setState({
      visible: false
    });
  };

  onDeleteClick = e => {
    const newList = this.state.data.filter(x => {
      return x._id !== e;
    });
    this.setState({ data: newList });
    axios
      .delete(`/profile/${e}`)
      .then(res => {
        alert("Delete success!");
      })
      .catch(err => {
        console.log(err);
      });
  };

  onFormSubmit = e => {
    e.preventDefault();
  };

  onInputChange = e => {
    this.setState({ searchByName: e.target.value });
  };

  searchSubmit = e => {
    if (!this.state.searchByName) {
      axios
        .get(
          `/profile/?isPagination=yes&pageSize=${this.state.pageSize}&page=${this.state.page}`
        )
        .then(res => {
          this.setState({
            data: res.data.query,
            loading: false,
            totalPages: res.data.pages
          });
        })
        .catch(function(err) {
          console.log(err);
        });
    }
    axios
      .get(`/auth/${this.state.searchByName}`)
      .then(res => {
        this.setState({ data: res.data, totalPages: res.data.length });
      })
      .catch(err => {
        console.log(err);
      });
  };

  onDeleteArray = e => {
    const target = this.state.profile.find(list => {
      return list._id === e;
    });
    // console.log(target);
    console.log(this.state.profile);
    const newProfile = this.state.profile.filter(list => {
      return list._id !== target._id;
    });
    // console.log(newProfile);
    this.setState({ profile: newProfile });
  };

  onEditArray = (id, name, phone, department, role) => {
    // console.log(phone);
    const index = this.state.data.findIndex(list => {
      return list._id === id;
    });
    const newData = this.state.data;
    newData[index] = {
      _id: id,
      name: name,
      phone: phone,
      department: department,
      role: role
    };
    this.setState({ data: newData });
  };

  onPageChange = page => {
    const { pageSize} = this.state;
    this.setState({loading:true})
    axios
      .get(
        `/profile/?isPagination=yes&pageSize=${pageSize}&page=${page}`
      )
      .then(res => {
        this.setState({
          data: res.data.query,
          loading:false
        });
      });
  };

  render() {
    return (
      <div>
        <ProfileById
          onFormSubmit={this.onFormSubmit}
          searchByName={this.state.searchByName}
          onInputChange={this.onInputChange}
          submit={this.searchSubmit}
        />
        <Spin spinning={this.state.loading}>
          <Table
            columns={this.state.columns}
            dataSource={this.state.data}
            rowKey={record => record.uid}
            pagination={false}
          />
          <Pagination
            defaultCurrent={this.state.page}
            defaultPageSize={this.state.pageSize}
            onChange={this.onPageChange}
            total={this.state.totalPages}
          />
          <Modal
            title={this.props.name}
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleOk}
            footer={null}
          >
            <ProfileEdit
              name={this.state.currentData.name}
              phone={this.state.currentData.phone}
              email={this.state.currentData._id}
              department={this.state.currentData.department}
              role={this.state.currentData.role}
              onEditArray={this.onEditArray}
              handleOk={this.handleOk}
            />
          </Modal>
        </Spin>
      </div>
    );
  }
}
