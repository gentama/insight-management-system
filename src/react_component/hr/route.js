import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Dashboard from './pages/Dashboard';
import accounts from './pages/accounts';
import department from './pages/Department';
import attendance from './pages/attendance';
import leave from './pages/leave';
import staffList from './pages/profile';
import error from '../404';
import insight from '../../Public/insight_logo.jpg';

function Index() {
  return (
    <div>
      <span style={{
        fontFamily: 'Segoe Script', fontSize: 36, position: 'absolute', left: '40%', top: '40%',
      }}
      >
        <img src={insight} alt="insight" style={{ height: 150, opacity: 0.8, margin: 'auto 0' }} />
        Insight
      </span>
    </div>
  );
}
export default () => (
  <Switch>
    <Route exact path="/hr" component={Index} />
    <Route exact path="/hr/Dashboard" component={Dashboard} />
    <Route exact path="/hr/accounts" component={accounts} />
    <Route exact path="/hr/department" component={department} />
    <Route exact path="/hr/attendance" component={attendance} />
    <Route exact path="/hr/leave" component={leave} />
    <Route exact path="/hr/staffList" component={staffList} />
    <Route component={error} />
  </Switch>
);
