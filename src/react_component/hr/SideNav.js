import React from 'react';
import { Link } from 'react-router-dom';
import { Drawer } from 'antd';
import '../../CSS/SideNav.scss';
import TopNav from '../TopNav';

export default class SideNav extends React.Component {
  constructor(props) {
    super(props);
    this.state = { visible: false };
  }

  render() {
    return (
      <div>
        <Drawer
          className="sideNavtt, abc"
          title="Basic Drawer"
          placement="left"
          closable={false}
          onClose={this.props.onClose}
          visible={this.props.visible}
        >
          <ul className="SideNav">
            <li>
              <Link to="/hr/Dashboard">Dashboard</Link>
            </li>
            <li>
              <Link to="/hr/staffList">StaffList</Link>
            </li>
            <li>
              <Link to="/hr/Attendance">Attendance</Link>
            </li>
            <li>
              <Link to="/hr/department">Department</Link>
            </li>
            <li>
              <Link to="/hr/leave">leave Requests</Link>
            </li>
            <li>
              <Link to="/hr/Accounts">Accounts</Link>
            </li>
          </ul>
        </Drawer>
        <TopNav showDrawer={this.props.showDrawer} />

      </div>
    );
  }
}
