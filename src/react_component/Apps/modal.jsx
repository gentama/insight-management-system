import React from 'react';
import { Button, Modal } from 'antd';

export default function modal() {
  return (
    <div>
      <Modal
        visible={this.state.visible}
        title="Title"
        onOk={this.handleOk}
        onCancel={this.handleCancel}
        footer={[
          <Button key="back" onClick={this.handleCancel}>
              Return
          </Button>,

          <Button
            key="submit"
            type="primary"
            loading={this.state.loading}
            onClick={this.handleOk}
          >
              Submit
          </Button>,
        ]}
      >
        <div className="modal_Form">
          <form onSubmit={this.onFormSubmit}>
            <div className="modal_Email">
              <span>Input your Email address:</span>
              <input
                name="email"
                type="email"
                value={this.state.email}
                onChange={this.handleInputChange}
                placeholder="Email address"
                required
              />
            </div>
            <div className="modal_Password">
              <span>Input your password:</span>
              <input
                name="password"
                type="password"
                value={this.state.password}
                onChange={this.handleInputChange}
                placeholder="password"
                required
              />
            </div>
          </form>
        </div>
      </Modal>
    </div>
  );
}
