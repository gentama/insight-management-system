export const AVATAR = 'AVATAR';

export const avatarChange = (avatar) => ({
  type: AVATAR,
  payload: avatar,
});