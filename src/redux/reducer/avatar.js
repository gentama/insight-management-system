const initialState = {
  avatar: '123',
};

export default function isLogin(state = initialState, action) {
  switch (action.type) {
    case 'AVATAR':
      return { ...state, avatar: action.payload.avatar };
    default:
      return state;
  }
}
