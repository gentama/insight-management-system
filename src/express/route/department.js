const express = require('express');

const router = express.Router();
const {
  addNewDepartment,
  getAllDepartment,
  getDepartmentUser,
  DeleteDepartment,
} = require('../controller/department');

router.get('/', getAllDepartment);
router.post('/', addNewDepartment);
router.get('/:dep', getDepartmentUser);
router.delete('/:id', DeleteDepartment);


module.exports = router;
