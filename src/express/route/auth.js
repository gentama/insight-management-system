const express = require('express');
const { loginUser } = require('../controller/auth');
const { getProfileByName } = require('../controller/profile');

const router = express.Router();

router.post('/', loginUser);
router.get('/:name', getProfileByName);


module.exports = router;
