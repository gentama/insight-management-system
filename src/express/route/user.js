const express = require('express');

const router = express.Router();
const {
  addNewUser,
  findUser,
  changeUserPassword,
} = require('../controller/user');

router.post('/', addNewUser);
router.get('/:email', findUser);
router.put('/:email', changeUserPassword);

module.exports = router;
