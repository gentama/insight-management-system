const express = require('express');
const imageUpload = require('../middleware/imageUpload');

const router = express.Router();
const {
  getOneStaffProfile,
  getAllStaffProfile,
  AddProfile,
  ChangeProfileAndImageUpload,
  DeleteProfile,
} = require('../controller/profile');

router.get('/', getAllStaffProfile);
router.get('/:email', getOneStaffProfile);
router.post('/', AddProfile);
router.put('/:id', imageUpload('file', 'id'), ChangeProfileAndImageUpload);
router.delete('/:id', DeleteProfile);

module.exports = router;
