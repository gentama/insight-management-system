const jwt = require('jsonwebtoken');

const generateToken = (id, auth) => {
  const token = jwt.sign({ id, auth }, process.env.JWT_SECRET, {
    expiresIn: '1h',
  });
  return token;
};

const validateToken = (token) => {
  let decoded;
  try {
    decoded = jwt.verify(token, process.env.JWT_SECRET);
  } catch (e) {
    return null;
  }
  return decoded;
};

module.exports = { generateToken, validateToken };
