const mongoose = require('mongoose');

exports.connectToDB = () => {
  const {
    DB_DATABASE, DB_USER, DB_PASSWORD, DB_HOST,
  } = process.env;

  const connectionString = `mongodb+srv://${DB_USER}:${DB_PASSWORD}@${DB_HOST}/${DB_DATABASE}`;

  mongoose.set('useFindAndModify', false);
  return mongoose.connect(connectionString, {
    useNewUrlParser: true,
    useCreateIndex: true,
  });
};
