const User = require('../model/profile');
const { generateToken } = require('../utils/jwt');

async function loginUser(req, res) {
  const { email, password } = req.body;
  const existUser = await User.findById(email).exec();
  if (!existUser) {
    return res.status(401).json('Invalid username or password');
  }
  const valiPassword = await existUser.validatePassword(password);
  if (!valiPassword) {
    return res.status(401).json('Invalid username or password');
  }
  const token = generateToken(existUser.email, existUser.user.auth);
  return res.json({ email, token });
}
module.exports = { loginUser };
