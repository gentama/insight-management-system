const bcrypt = require('bcrypt');
const User = require('../model/profile');
const Department = require('../model/department');
const { generateToken } = require('../utils/jwt');

async function addNewUser(req, res) {
  const {
    email, user, name, phone, department, role, avatar,
  } = req.body;
  const { password, auth } = user;
  const isUserExist = await User.findById(email).exec();
  const targetDepartment = await Department.findOne({ name: department }).exec();
  if (isUserExist) {
    return res.status(400).json('user already exist');
  }
  const users = new User({
    name,
    phone,
    department,
    role,
    email,
    avatar,
    user,
    password,
    auth,
  });
  await users.hashPassword();
  await users.save();
  targetDepartment.profile.addToSet(email);
  await targetDepartment.save();
  const token = generateToken(users.email);
  return res.json({ email, token });
}


async function findUser(req, res) {
  const { email } = req.params;

  const user = await User.findById(email);
  if (!user) {
    return res.status(404).json('user not found');
  }
  return res.json(user);
}

async function changeUserPassword(req, res) {
  const { email } = req.params;
  const { user } = req.body;
  let { password } = user;
  password = await bcrypt.hash(password, 10);
  const newPassword = await User.findByIdAndUpdate(
    email, { $set: { 'user.password': password } }, { new: true },
  );
  if (!newPassword) {
    return res.status(404).json('profile not found');
  }
  return res.json(newPassword);
}

module.exports = {
  addNewUser,
  findUser,
  changeUserPassword,
};
