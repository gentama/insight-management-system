const Leave = require('../model/leaveRequest');
const Profile = require('../model/profile');

async function getOneLeaveRequest(req, res) {
  const { id } = req.params;

  const leave = await Leave.findById(id);
  if (!leave) {
    return res.status(404).json('Request not found');
  }
  return res.json(leave);
}

async function getAllLeaveRequest(req, res) {
  const leave = await Leave.find();
  return res.json(leave);
}

function convertTime(start, end) {
  const x = new Date(start);
  const y = new Date(end);
  return parseInt(y - x, 10) / 1000 / 3600 / 24;
}

async function AddLeaveRequest(req, res) {
  const {
    id,
    email,
    name,
    type,
    reason,
    StartDate,
    EndDate,
    isApproved,
  } = req.body;
  const searchRequest = await Leave.findById(id);
  const total = convertTime(StartDate, EndDate);
  if (searchRequest) {
    return res.status(400).json('Request id already existed');
  }
  const leave = new Leave({
    id,
    email,
    name,
    type,
    reason,
    StartDate,
    EndDate,
    total,
    isApproved,
  });
  const profile = await Profile.findById(email);
  if (!profile) {
    return res.status(404).json('profile not found');
  }
  profile.leave.addToSet(id);

  await profile.save();
  await leave.save();
  return res.json(profile);
}

async function ChangeLeaveRequest(req, res) {
  const { id } = req.params;
  const { isApproved } = req.body;
  const newLeave = await Leave.findByIdAndUpdate(
    id,
    { isApproved },
    { new: true },
  );
  if (!newLeave) {
    return res.status(404).json('Request not found');
  }
  return res.json(newLeave);
}

async function DeleteLeaveRequest(req, res) {
  const { id } = req.params;
  const leave = await Leave.findByIdAndDelete(id);
  if (!leave) {
    return res.status(404).json('Request not found');
  }
  return res.send('Delete succeed!');
}

module.exports = {
  getOneLeaveRequest,
  getAllLeaveRequest,
  AddLeaveRequest,
  ChangeLeaveRequest,
  DeleteLeaveRequest,
};
