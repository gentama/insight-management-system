const Profile = require('../model/profile');

async function getOneStaffProfile(req, res) {
  const { email } = req.params;

  const profile = await Profile.findById(email).populate(
    'leave',
    'type reason StartDate EndDate isApproved',
  );
  if (!profile) {
    return res.status(404).json('profile not found');
  }
  return res.json(profile);
}

async function getProfileByName(req, res) {
  const { name } = req.params;

  const profile = await Profile.find({ name }).populate(
    'leave',
    'type reason StartDate EndDate isApproved total',
  );
  if (!profile) {
    return res.status(404).json('profile not found');
  }
  return res.json(profile);
}

async function getAllStaffProfile(req, res) {
  const profile = await Profile.find().populate(
    'leave',
    'email name type reason StartDate EndDate isApproved total',
  );
  const convertPagination = (query, total) => {
    if (total === 0) {
      return { page: 1, pageSize: 10, pages: 1 };
    }
    let { pageSize, page } = query;
    pageSize = parseInt(query.pageSize, 10) || 10;
    page = parseInt(query.page, 10) || 1;
    if (page < 1) {
      page = 1;
    }
    const pages = Math.ceil(total / pageSize);
    if (page > pages) {
      page = pages;
    }
    return { page, pageSize, pages };
  };
  const total = await Profile.find().countDocuments();
  const { page, pageSize } = convertPagination(req.query, total);
  const query = await Profile.find().populate(
    'leave',
    'email name type reason StartDate EndDate isApproved total',
  ).skip((page - 1) * pageSize).limit(pageSize);
  const { isPagination } = req.query;
  if (isPagination === 'yes') {
    return res.json({ query, pages: total });
  }
  if (isPagination === 'no') {
    return res.json(profile);
  }
  return res.status(500).json('isPagination pram is required');
}

async function AddProfile(req, res) {
  const {
    name, email, phone, department, role,
  } = req.body;

  const searchProfile = await Profile.findById(email);
  if (searchProfile) {
    return res.status(400).json('Profile already existed');
  }
  const profile = new Profile({
    name,
    email,
    phone,
    department,
    role,
  });
  await profile.save();
  return res.json(profile);
}

async function ChangeProfileAndImageUpload(req, res) {
  const { id } = req.params;
  const {
    name, phone, department, role, email,
  } = req.body;
  const avatar = req.file
    ? req.file.location
    : (await Profile.findById(id)).avatar;
  const newProfile = await Profile.findByIdAndUpdate(
    id,
    {
      $set: {
        name,
        phone,
        department,
        role,
        email,
        avatar,
      },
    },
    { new: true },
  );
  if (!newProfile) {
    return res.status(404).json('profile not found');
  }
  return res.json(newProfile);
}

async function DeleteProfile(req, res) {
  const { id } = req.params;
  const profile = await Profile.findByIdAndDelete(id);
  if (!profile) {
    return res.status(404).json('profile not found');
  }
  return res.send('Delete succeed!');
}

module.exports = {
  getOneStaffProfile,
  getAllStaffProfile,
  AddProfile,
  ChangeProfileAndImageUpload,
  DeleteProfile,
  getProfileByName,
};
