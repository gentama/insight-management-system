const Department = require('../model/department');
const Profile = require('../model/profile');

async function addNewDepartment(req, res) {
  const {
    id, name, head, key,
  } = req.body;
  const department = new Department({
    id, name, head, key,
  });
  await department.save();
  return res.json({ department });
}


async function getAllDepartment(req, res) {
  const department = await Department.find().populate(
    'profile',
    '_id name role phone',
  );
  return res.json(department);
}

async function DeleteDepartment(req, res) {
  const { id } = req.params;
  const profile = await Department.findByIdAndDelete(id);
  if (!profile) {
    return res.status(404).json('profile not found');
  }
  return res.send('Delete succeed!');
}

async function getDepartmentUser(req, res) {
  const { dep } = req.params;
  const total = await Profile.find({ department: dep });
  return res.json(total.length);
}

module.exports = {
  addNewDepartment,
  getAllDepartment,
  getDepartmentUser,
  DeleteDepartment,
};
