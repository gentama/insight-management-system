const express = require('express');
const profile = require('./route/profile');
const leaveRequest = require('./route/leaveRequest');
const leaveCheck = require('./route/leavecheck');
const dashBoard = require('./route/dashBoard');
const user = require('./route/user');
const auth = require('./route/auth');
const dep = require('./route/department');
const authGuard = require('./middleware/authGuard');

const router = express.Router();

router.use('/profile', profile);
router.use('/leave', authGuard, leaveRequest);
router.use('/leavecheck', authGuard, leaveCheck);
router.use('/dashboard', authGuard, dashBoard);
router.use('/user', authGuard, user);
router.use('/auth', auth);
router.use('/dep', authGuard, dep);

module.exports = router;
