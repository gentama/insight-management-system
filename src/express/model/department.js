const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  _id: {
    type: String,
    require: true,
    alias: 'id',
  },
  key: {
    type: String,
    require: true,
  },
  name: {
    type: String,
    require: true,
  },
  head: {
    type: String,
    require: true,
  },
  profile: [{
    type: String,
    ref: 'Profile',
  }],
  __v: { type: Number, select: false },
});

const model = mongoose.model('Department', schema);

module.exports = model;
