const mongoose = require('mongoose');

const schema = new mongoose.Schema(
  {
    _id: {
      type: String,
      required: true,
      alias: 'id',
    },
    name: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    StartDate: {
      type: String,
      require: true,
    },
    EndDate: {
      type: String,
      require: true,
    },
    total: {
      type: Number,
      require: true,
    },
    reason: {
      type: String,
      require: true,
    },
    isApproved: {
      type: String,
      require: true,
    },
    __v: { type: Number, select: false },
  },
  {
    timestamps: true,
  },
);

const model = mongoose.model('Leave', schema);

module.exports = model;
