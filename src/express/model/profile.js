const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const schema = new mongoose.Schema(
  {
    _id: {
      type: String,
      required: true,
      alias: 'email',
    },
    name: {
      type: String,
      require: true,
    },
    phone: {
      type: String,
      require: true,
    },
    department: {
      type: String,
      require: true,
    },
    role: {
      type: String,
      require: true,
    },
    avatar: {
      type: String,
      require: true,
    },
    leave: [
      {
        type: String,
        ref: 'Leave',
      },
    ],
    user: {
      password: {
        type: String,
        require: true,
      },
      auth: {
        type: String,
        require: true,
      },
    },
    __v: { type: Number, select: false },
  },
  {
    timestamps: true,
  },
);
// profile.hasPassword()
schema.methods.hashPassword = async function () {
  this.user.password = await bcrypt.hash(this.user.password, 10);
};

schema.methods.validatePassword = async function (password) {
  const validatePassword = await bcrypt.compare(password, this.user.password);
  return validatePassword;
};

const model = mongoose.model('Profile', schema);

module.exports = model;
