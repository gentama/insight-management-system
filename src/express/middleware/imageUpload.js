const aws = require('aws-sdk');
const multerS3 = require('multer-s3');
const multer = require('multer');

const ONE_MEGEBYTE = 1048576;

// eslint-disable-next-line camelcase
const { AWS_kEY, AWS_SECRET } = process.env;

const creds = new aws.Credentials({
  accessKeyId: AWS_kEY,
  secretAccessKey: AWS_SECRET,
});
const s3 = new aws.S3({
  region: 'ap-southeast-2',
  credentials: creds,
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb({ name: 'MulterError', message: 'Only support common image file' });
  }
};

module.exports = (fieldName, urlParamName) => {
  const storage = multerS3({
    s3,
    bucket: 'insightdemo-image',
    key(req, file, cb) {
      const type = file.mimetype
        .split('')
        .splice(6)
        .join('');
      const fileName = (urlParamName && req.params[urlParamName]) || Date.now().toString();
      const fullPath = `${fileName}.${type}`;
      cb(null, fullPath);
    },
  });
  // const storage = multer.memoryStorage()
  const parser = multer({
    fileFilter,
    storage,
    limits: {
      fileSize: ONE_MEGEBYTE,
    },
  });

  const middleware = parser.single(fieldName);

  return middleware;
};
