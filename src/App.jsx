/* eslint-disable import/no-named-as-default */
import React from 'react';
import Routes from './react_component/Routes';

export default function App() {
  return (
    <div>
      <Routes />
    </div>
  );
}
